﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameCollegeVotingApp;

namespace VotingApp_Test
{
  [TestClass]
  public class UnitTest1
  {
    GameCollegeVotingApp.VotingApp voteapp;
    
    [TestInitialize]
    public void InitialiseerVotingApp()
    {
      voteapp = new VotingApp();
    }

    [TestMethod]
    public void TestBezoeker()
    {
      voteapp.Aanmaken("joris.sonck@student.kdg.be", "wachtwoord");
      Assert.IsTrue(voteapp.GetBezoeker() != null);
    }

    [TestMethod]
    public void TestAantalVotes()
    {
      voteapp.Aanmaken("joris.sonck@student.kdg.be", "wachtwoord");
      Assert.IsTrue(voteapp.GetBezoeker().Votes <= 3);
    }

    [TestMethod]
    public void TestDemos()
    {
            
    }
  }
}
