﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeVotingApp
{
  public class GameDemo
  {//[]
    protected string mNaam;
    protected List<Student> mStudents = new List<Student>();

    public GameDemo(Student s1, Student s2, Student s3, string naam)
    {
      mNaam = naam;
      mStudents.Add(s1);
      mStudents.Add(s2);
      mStudents.Add(s3);
    }
    public string Naam
    {
      get { return mNaam; } 
    }
    public List<Student> GetStudents()
    {
      return mStudents;
    }
    

    public string StudentNaam(int nrStudent)
    {
      string naam;
      return naam = (mStudents[nrStudent - 1].Voornaam + " " + mStudents[nrStudent -1].Achternaam);
    }
  }
}
