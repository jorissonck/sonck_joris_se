﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeVotingApp
{
 public class Bezoeker
  {
   protected string mEmail;
   protected string mWachtwoord;
   protected int maxVotes;
   protected int votes;

   public Bezoeker(string email, string wachtwoord)
   {
     mEmail = email;
     mWachtwoord = wachtwoord;
     maxVotes = 3;
     votes = 0;
   }
   public string Email
   {
     get { return mEmail; }
   }

   public int Votes
   {
     get { return votes; }
     set { votes = value; }
   }
   public int MaxVotes
   {
     get { return maxVotes; }
   }

  }
}
