﻿namespace GameCollegeVotingApp
{
  partial class frmMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.voteAppUI2 = new GameCollegeVotingApp.VoteAppUI();
      this.voteAppUI1 = new GameCollegeVotingApp.VoteAppUI();
      this.voteApp1 = new GameCollegeVotingApp.VoteAppUI();
      this.SuspendLayout();
      // 
      // voteAppUI2
      // 
      this.voteAppUI2.AutoSize = true;
      this.voteAppUI2.Location = new System.Drawing.Point(0, 0);
      this.voteAppUI2.Name = "voteAppUI2";
      this.voteAppUI2.Size = new System.Drawing.Size(303, 516);
      this.voteAppUI2.TabIndex = 1;
      // 
      // voteAppUI1
      // 
      this.voteAppUI1.AutoSize = true;
      this.voteAppUI1.Location = new System.Drawing.Point(12, 12);
      this.voteAppUI1.Name = "voteAppUI1";
      this.voteAppUI1.Size = new System.Drawing.Size(1204, 516);
      this.voteAppUI1.TabIndex = 0;
      // 
      // voteApp1
      // 
      this.voteApp1.AutoSize = true;
      this.voteApp1.Location = new System.Drawing.Point(12, 5);
      this.voteApp1.Name = "voteApp1";
      this.voteApp1.Size = new System.Drawing.Size(1204, 513);
      this.voteApp1.TabIndex = 0;
      // 
      // frmMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(288, 509);
      this.Controls.Add(this.voteAppUI2);
      this.Controls.Add(this.voteAppUI1);
      this.MaximumSize = new System.Drawing.Size(306, 556);
      this.MinimumSize = new System.Drawing.Size(306, 556);
      this.Name = "frmMainForm";
      this.Text = "VotingApp";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private VoteAppUI voteApp1;
    private VoteAppUI voteAppUI1;
    private VoteAppUI voteAppUI2;
  }
}

