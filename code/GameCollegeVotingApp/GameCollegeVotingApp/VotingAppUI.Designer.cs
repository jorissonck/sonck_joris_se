﻿namespace GameCollegeVotingApp
{
  partial class VoteAppUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoteAppUI));
      this.btnRegistreren = new System.Windows.Forms.Button();
      this.grpRegistreren = new System.Windows.Forms.GroupBox();
      this.label6 = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.grpGegevens = new System.Windows.Forms.GroupBox();
      this.lblError = new System.Windows.Forms.Label();
      this.pictureBox2 = new System.Windows.Forms.PictureBox();
      this.txtWachtwoord = new System.Windows.Forms.TextBox();
      this.txtEmail = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.btnAccountAanmaken = new System.Windows.Forms.Button();
      this.picLogo = new System.Windows.Forms.PictureBox();
      this.grpGamedemos = new System.Windows.Forms.GroupBox();
      this.lblMyVotes = new System.Windows.Forms.Label();
      this.picDemo6 = new System.Windows.Forms.PictureBox();
      this.picDemo4 = new System.Windows.Forms.PictureBox();
      this.picDemo5 = new System.Windows.Forms.PictureBox();
      this.picDemo2 = new System.Windows.Forms.PictureBox();
      this.picDemo3 = new System.Windows.Forms.PictureBox();
      this.picDemo1 = new System.Windows.Forms.PictureBox();
      this.label3 = new System.Windows.Forms.Label();
      this.grpVotes = new System.Windows.Forms.GroupBox();
      this.lblMyVotes2 = new System.Windows.Forms.Label();
      this.btnVorige = new System.Windows.Forms.Button();
      this.btnVoteStudent3 = new System.Windows.Forms.Button();
      this.btnVoteStudent2 = new System.Windows.Forms.Button();
      this.btnVoteStudent1 = new System.Windows.Forms.Button();
      this.lblStudent3 = new System.Windows.Forms.Label();
      this.lblStudent2 = new System.Windows.Forms.Label();
      this.lblStudent1 = new System.Windows.Forms.Label();
      this.pictureBox3 = new System.Windows.Forms.PictureBox();
      this.label4 = new System.Windows.Forms.Label();
      this.lblNaamGameDemo = new System.Windows.Forms.Label();
      this.lblWelkom = new System.Windows.Forms.Label();
      this.btnMyVotes = new System.Windows.Forms.Button();
      this.grpRegistreren.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.grpGegevens.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
      this.grpGamedemos.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo6)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo1)).BeginInit();
      this.grpVotes.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
      this.SuspendLayout();
      // 
      // btnRegistreren
      // 
      this.btnRegistreren.AllowDrop = true;
      this.btnRegistreren.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnRegistreren.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRegistreren.ForeColor = System.Drawing.Color.White;
      this.btnRegistreren.Location = new System.Drawing.Point(69, 417);
      this.btnRegistreren.Name = "btnRegistreren";
      this.btnRegistreren.Size = new System.Drawing.Size(147, 39);
      this.btnRegistreren.TabIndex = 0;
      this.btnRegistreren.Text = "Registreren";
      this.btnRegistreren.UseVisualStyleBackColor = false;
      this.btnRegistreren.Click += new System.EventHandler(this.btnRegistreren_Click);
      // 
      // grpRegistreren
      // 
      this.grpRegistreren.BackColor = System.Drawing.Color.CornflowerBlue;
      this.grpRegistreren.Controls.Add(this.label6);
      this.grpRegistreren.Controls.Add(this.btnRegistreren);
      this.grpRegistreren.Controls.Add(this.pictureBox1);
      this.grpRegistreren.Location = new System.Drawing.Point(296, 0);
      this.grpRegistreren.Name = "grpRegistreren";
      this.grpRegistreren.Size = new System.Drawing.Size(290, 510);
      this.grpRegistreren.TabIndex = 1;
      this.grpRegistreren.TabStop = false;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label6.Location = new System.Drawing.Point(64, 281);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(147, 30);
      this.label6.TabIndex = 2;
      this.label6.Text = "Voting App";
      // 
      // pictureBox1
      // 
      this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
      this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.pictureBox1.Location = new System.Drawing.Point(41, 21);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(194, 228);
      this.pictureBox1.TabIndex = 1;
      this.pictureBox1.TabStop = false;
      // 
      // grpGegevens
      // 
      this.grpGegevens.BackColor = System.Drawing.Color.CornflowerBlue;
      this.grpGegevens.Controls.Add(this.lblError);
      this.grpGegevens.Controls.Add(this.pictureBox2);
      this.grpGegevens.Controls.Add(this.txtWachtwoord);
      this.grpGegevens.Controls.Add(this.txtEmail);
      this.grpGegevens.Controls.Add(this.label2);
      this.grpGegevens.Controls.Add(this.label1);
      this.grpGegevens.Controls.Add(this.btnAccountAanmaken);
      this.grpGegevens.Location = new System.Drawing.Point(595, 0);
      this.grpGegevens.Name = "grpGegevens";
      this.grpGegevens.Size = new System.Drawing.Size(290, 510);
      this.grpGegevens.TabIndex = 2;
      this.grpGegevens.TabStop = false;
      this.grpGegevens.Visible = false;
      // 
      // lblError
      // 
      this.lblError.AutoSize = true;
      this.lblError.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblError.ForeColor = System.Drawing.Color.Red;
      this.lblError.Location = new System.Drawing.Point(26, 340);
      this.lblError.Name = "lblError";
      this.lblError.Size = new System.Drawing.Size(214, 17);
      this.lblError.TabIndex = 5;
      this.lblError.Text = "Vul alle velden correct in aub!";
      this.lblError.Visible = false;
      // 
      // pictureBox2
      // 
      this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
      this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.pictureBox2.Location = new System.Drawing.Point(87, 21);
      this.pictureBox2.Name = "pictureBox2";
      this.pictureBox2.Size = new System.Drawing.Size(100, 111);
      this.pictureBox2.TabIndex = 1;
      this.pictureBox2.TabStop = false;
      // 
      // txtWachtwoord
      // 
      this.txtWachtwoord.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtWachtwoord.Location = new System.Drawing.Point(43, 305);
      this.txtWachtwoord.Name = "txtWachtwoord";
      this.txtWachtwoord.Size = new System.Drawing.Size(170, 23);
      this.txtWachtwoord.TabIndex = 4;
      // 
      // txtEmail
      // 
      this.txtEmail.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtEmail.Location = new System.Drawing.Point(43, 255);
      this.txtEmail.Name = "txtEmail";
      this.txtEmail.Size = new System.Drawing.Size(170, 23);
      this.txtEmail.TabIndex = 4;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label2.Location = new System.Drawing.Point(40, 285);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(105, 19);
      this.label2.TabIndex = 3;
      this.label2.Text = "Wachtwoord:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label1.Location = new System.Drawing.Point(40, 230);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(56, 19);
      this.label1.TabIndex = 2;
      this.label1.Text = "e-mail:";
      // 
      // btnAccountAanmaken
      // 
      this.btnAccountAanmaken.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnAccountAanmaken.ForeColor = System.Drawing.Color.White;
      this.btnAccountAanmaken.Location = new System.Drawing.Point(78, 392);
      this.btnAccountAanmaken.Name = "btnAccountAanmaken";
      this.btnAccountAanmaken.Size = new System.Drawing.Size(124, 52);
      this.btnAccountAanmaken.TabIndex = 0;
      this.btnAccountAanmaken.Text = "Account Aanmaken";
      this.btnAccountAanmaken.UseVisualStyleBackColor = false;
      this.btnAccountAanmaken.Click += new System.EventHandler(this.btnAccountAanmaken_Click);
      // 
      // picLogo
      // 
      this.picLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picLogo.BackgroundImage")));
      this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picLogo.Location = new System.Drawing.Point(102, 41);
      this.picLogo.Name = "picLogo";
      this.picLogo.Size = new System.Drawing.Size(100, 111);
      this.picLogo.TabIndex = 1;
      this.picLogo.TabStop = false;
      // 
      // grpGamedemos
      // 
      this.grpGamedemos.BackColor = System.Drawing.Color.CornflowerBlue;
      this.grpGamedemos.Controls.Add(this.btnMyVotes);
      this.grpGamedemos.Controls.Add(this.lblWelkom);
      this.grpGamedemos.Controls.Add(this.lblMyVotes);
      this.grpGamedemos.Controls.Add(this.picDemo6);
      this.grpGamedemos.Controls.Add(this.picDemo4);
      this.grpGamedemos.Controls.Add(this.picDemo5);
      this.grpGamedemos.Controls.Add(this.picDemo2);
      this.grpGamedemos.Controls.Add(this.picDemo3);
      this.grpGamedemos.Controls.Add(this.picDemo1);
      this.grpGamedemos.Controls.Add(this.label3);
      this.grpGamedemos.Controls.Add(this.picLogo);
      this.grpGamedemos.Location = new System.Drawing.Point(891, 0);
      this.grpGamedemos.Name = "grpGamedemos";
      this.grpGamedemos.Size = new System.Drawing.Size(290, 510);
      this.grpGamedemos.TabIndex = 3;
      this.grpGamedemos.TabStop = false;
      this.grpGamedemos.Visible = false;
      // 
      // lblMyVotes
      // 
      this.lblMyVotes.AutoSize = true;
      this.lblMyVotes.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblMyVotes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
      this.lblMyVotes.Location = new System.Drawing.Point(195, 187);
      this.lblMyVotes.Name = "lblMyVotes";
      this.lblMyVotes.Size = new System.Drawing.Size(76, 17);
      this.lblMyVotes.TabIndex = 5;
      this.lblMyVotes.Text = "Votes: 0/3";
      // 
      // picDemo6
      // 
      this.picDemo6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picDemo6.BackgroundImage")));
      this.picDemo6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picDemo6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.picDemo6.Location = new System.Drawing.Point(177, 426);
      this.picDemo6.Name = "picDemo6";
      this.picDemo6.Size = new System.Drawing.Size(63, 66);
      this.picDemo6.TabIndex = 4;
      this.picDemo6.TabStop = false;
      this.picDemo6.Click += new System.EventHandler(this.picDemo6_Click);
      // 
      // picDemo4
      // 
      this.picDemo4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picDemo4.BackgroundImage")));
      this.picDemo4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picDemo4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.picDemo4.Location = new System.Drawing.Point(177, 344);
      this.picDemo4.Name = "picDemo4";
      this.picDemo4.Size = new System.Drawing.Size(63, 66);
      this.picDemo4.TabIndex = 4;
      this.picDemo4.TabStop = false;
      this.picDemo4.Click += new System.EventHandler(this.picDemo4_Click);
      // 
      // picDemo5
      // 
      this.picDemo5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picDemo5.BackgroundImage")));
      this.picDemo5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picDemo5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.picDemo5.Location = new System.Drawing.Point(62, 426);
      this.picDemo5.Name = "picDemo5";
      this.picDemo5.Size = new System.Drawing.Size(63, 66);
      this.picDemo5.TabIndex = 4;
      this.picDemo5.TabStop = false;
      this.picDemo5.Click += new System.EventHandler(this.picDemo5_Click);
      // 
      // picDemo2
      // 
      this.picDemo2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picDemo2.BackgroundImage")));
      this.picDemo2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picDemo2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.picDemo2.Location = new System.Drawing.Point(177, 255);
      this.picDemo2.Name = "picDemo2";
      this.picDemo2.Size = new System.Drawing.Size(63, 66);
      this.picDemo2.TabIndex = 4;
      this.picDemo2.TabStop = false;
      this.picDemo2.Click += new System.EventHandler(this.picDemo2_Click);
      // 
      // picDemo3
      // 
      this.picDemo3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picDemo3.BackgroundImage")));
      this.picDemo3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picDemo3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.picDemo3.Location = new System.Drawing.Point(62, 344);
      this.picDemo3.Name = "picDemo3";
      this.picDemo3.Size = new System.Drawing.Size(63, 66);
      this.picDemo3.TabIndex = 4;
      this.picDemo3.TabStop = false;
      this.picDemo3.Click += new System.EventHandler(this.picDemo3_Click);
      // 
      // picDemo1
      // 
      this.picDemo1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picDemo1.BackgroundImage")));
      this.picDemo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picDemo1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.picDemo1.Location = new System.Drawing.Point(62, 255);
      this.picDemo1.Name = "picDemo1";
      this.picDemo1.Size = new System.Drawing.Size(63, 66);
      this.picDemo1.TabIndex = 4;
      this.picDemo1.TabStop = false;
      this.picDemo1.Click += new System.EventHandler(this.picDemo1_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label3.Location = new System.Drawing.Point(9, 217);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(106, 19);
      this.label3.TabIndex = 3;
      this.label3.Text = "Gamedemo\'s:";
      // 
      // grpVotes
      // 
      this.grpVotes.BackColor = System.Drawing.Color.CornflowerBlue;
      this.grpVotes.Controls.Add(this.lblNaamGameDemo);
      this.grpVotes.Controls.Add(this.lblMyVotes2);
      this.grpVotes.Controls.Add(this.btnVorige);
      this.grpVotes.Controls.Add(this.btnVoteStudent3);
      this.grpVotes.Controls.Add(this.btnVoteStudent2);
      this.grpVotes.Controls.Add(this.btnVoteStudent1);
      this.grpVotes.Controls.Add(this.lblStudent3);
      this.grpVotes.Controls.Add(this.lblStudent2);
      this.grpVotes.Controls.Add(this.lblStudent1);
      this.grpVotes.Controls.Add(this.pictureBox3);
      this.grpVotes.Controls.Add(this.label4);
      this.grpVotes.Location = new System.Drawing.Point(0, 0);
      this.grpVotes.Name = "grpVotes";
      this.grpVotes.Size = new System.Drawing.Size(290, 510);
      this.grpVotes.TabIndex = 4;
      this.grpVotes.TabStop = false;
      this.grpVotes.Visible = false;
      // 
      // lblMyVotes2
      // 
      this.lblMyVotes2.AutoSize = true;
      this.lblMyVotes2.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblMyVotes2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
      this.lblMyVotes2.Location = new System.Drawing.Point(213, 169);
      this.lblMyVotes2.Name = "lblMyVotes2";
      this.lblMyVotes2.Size = new System.Drawing.Size(76, 17);
      this.lblMyVotes2.TabIndex = 5;
      this.lblMyVotes2.Text = "Votes: 0/3";
      // 
      // btnVorige
      // 
      this.btnVorige.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnVorige.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnVorige.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.btnVorige.Location = new System.Drawing.Point(29, 97);
      this.btnVorige.Name = "btnVorige";
      this.btnVorige.Size = new System.Drawing.Size(44, 35);
      this.btnVorige.TabIndex = 6;
      this.btnVorige.Text = "<<";
      this.btnVorige.UseVisualStyleBackColor = false;
      this.btnVorige.Click += new System.EventHandler(this.btnVorige_Click);
      // 
      // btnVoteStudent3
      // 
      this.btnVoteStudent3.BackColor = System.Drawing.Color.YellowGreen;
      this.btnVoteStudent3.Location = new System.Drawing.Point(210, 397);
      this.btnVoteStudent3.Name = "btnVoteStudent3";
      this.btnVoteStudent3.Size = new System.Drawing.Size(75, 23);
      this.btnVoteStudent3.TabIndex = 5;
      this.btnVoteStudent3.Text = "Vote!";
      this.btnVoteStudent3.UseVisualStyleBackColor = false;
      this.btnVoteStudent3.Click += new System.EventHandler(this.btnVoteStudent3_Click);
      // 
      // btnVoteStudent2
      // 
      this.btnVoteStudent2.BackColor = System.Drawing.Color.YellowGreen;
      this.btnVoteStudent2.Location = new System.Drawing.Point(210, 340);
      this.btnVoteStudent2.Name = "btnVoteStudent2";
      this.btnVoteStudent2.Size = new System.Drawing.Size(75, 23);
      this.btnVoteStudent2.TabIndex = 5;
      this.btnVoteStudent2.Text = "Vote!";
      this.btnVoteStudent2.UseVisualStyleBackColor = false;
      this.btnVoteStudent2.Click += new System.EventHandler(this.btnVoteStudent2_Click);
      // 
      // btnVoteStudent1
      // 
      this.btnVoteStudent1.BackColor = System.Drawing.Color.YellowGreen;
      this.btnVoteStudent1.Location = new System.Drawing.Point(210, 285);
      this.btnVoteStudent1.Name = "btnVoteStudent1";
      this.btnVoteStudent1.Size = new System.Drawing.Size(75, 23);
      this.btnVoteStudent1.TabIndex = 5;
      this.btnVoteStudent1.Text = "Vote!";
      this.btnVoteStudent1.UseVisualStyleBackColor = false;
      this.btnVoteStudent1.Click += new System.EventHandler(this.btnVoteStudent1_Click);
      // 
      // lblStudent3
      // 
      this.lblStudent3.AutoSize = true;
      this.lblStudent3.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblStudent3.ForeColor = System.Drawing.Color.White;
      this.lblStudent3.Location = new System.Drawing.Point(26, 400);
      this.lblStudent3.Name = "lblStudent3";
      this.lblStudent3.Size = new System.Drawing.Size(78, 19);
      this.lblStudent3.TabIndex = 4;
      this.lblStudent3.Text = "Student3";
      // 
      // lblStudent2
      // 
      this.lblStudent2.AutoSize = true;
      this.lblStudent2.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblStudent2.ForeColor = System.Drawing.Color.White;
      this.lblStudent2.Location = new System.Drawing.Point(26, 343);
      this.lblStudent2.Name = "lblStudent2";
      this.lblStudent2.Size = new System.Drawing.Size(78, 19);
      this.lblStudent2.TabIndex = 4;
      this.lblStudent2.Text = "Student2";
      // 
      // lblStudent1
      // 
      this.lblStudent1.AutoSize = true;
      this.lblStudent1.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblStudent1.ForeColor = System.Drawing.Color.White;
      this.lblStudent1.Location = new System.Drawing.Point(26, 288);
      this.lblStudent1.Name = "lblStudent1";
      this.lblStudent1.Size = new System.Drawing.Size(78, 19);
      this.lblStudent1.TabIndex = 4;
      this.lblStudent1.Text = "Student1";
      // 
      // pictureBox3
      // 
      this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
      this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.pictureBox3.Location = new System.Drawing.Point(117, 21);
      this.pictureBox3.Name = "pictureBox3";
      this.pictureBox3.Size = new System.Drawing.Size(100, 111);
      this.pictureBox3.TabIndex = 1;
      this.pictureBox3.TabStop = false;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label4.Location = new System.Drawing.Point(26, 230);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(85, 19);
      this.label4.TabIndex = 3;
      this.label4.Text = "Studenten:";
      // 
      // lblNaamGameDemo
      // 
      this.lblNaamGameDemo.AutoSize = true;
      this.lblNaamGameDemo.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblNaamGameDemo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.lblNaamGameDemo.Location = new System.Drawing.Point(29, 169);
      this.lblNaamGameDemo.Name = "lblNaamGameDemo";
      this.lblNaamGameDemo.Size = new System.Drawing.Size(98, 17);
      this.lblNaamGameDemo.TabIndex = 7;
      this.lblNaamGameDemo.Text = "GameDemo:";
      // 
      // lblWelkom
      // 
      this.lblWelkom.AutoSize = true;
      this.lblWelkom.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblWelkom.ForeColor = System.Drawing.SystemColors.Control;
      this.lblWelkom.Location = new System.Drawing.Point(7, 21);
      this.lblWelkom.Name = "lblWelkom";
      this.lblWelkom.Size = new System.Drawing.Size(112, 17);
      this.lblWelkom.TabIndex = 6;
      this.lblWelkom.Text = "Welkom, email";
      // 
      // btnMyVotes
      // 
      this.btnMyVotes.AllowDrop = true;
      this.btnMyVotes.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnMyVotes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.btnMyVotes.Location = new System.Drawing.Point(13, 103);
      this.btnMyVotes.Name = "btnMyVotes";
      this.btnMyVotes.Size = new System.Drawing.Size(75, 53);
      this.btnMyVotes.TabIndex = 7;
      this.btnMyVotes.Text = "Mijn Votes";
      this.btnMyVotes.UseVisualStyleBackColor = false;
      this.btnMyVotes.Click += new System.EventHandler(this.btnMyVotes_Click);
      // 
      // VoteAppUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.Controls.Add(this.grpVotes);
      this.Controls.Add(this.grpGamedemos);
      this.Controls.Add(this.grpGegevens);
      this.Controls.Add(this.grpRegistreren);
      this.Name = "VoteAppUI";
      this.Size = new System.Drawing.Size(1340, 516);
      this.grpRegistreren.ResumeLayout(false);
      this.grpRegistreren.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.grpGegevens.ResumeLayout(false);
      this.grpGegevens.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
      this.grpGamedemos.ResumeLayout(false);
      this.grpGamedemos.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo6)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picDemo1)).EndInit();
      this.grpVotes.ResumeLayout(false);
      this.grpVotes.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnRegistreren;
    private System.Windows.Forms.GroupBox grpRegistreren;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.GroupBox grpGegevens;
    private System.Windows.Forms.TextBox txtWachtwoord;
    private System.Windows.Forms.TextBox txtEmail;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.PictureBox picLogo;
    private System.Windows.Forms.Button btnAccountAanmaken;
    private System.Windows.Forms.PictureBox pictureBox2;
    private System.Windows.Forms.GroupBox grpGamedemos;
    private System.Windows.Forms.Label lblError;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.PictureBox picDemo6;
    private System.Windows.Forms.PictureBox picDemo4;
    private System.Windows.Forms.PictureBox picDemo5;
    private System.Windows.Forms.PictureBox picDemo2;
    private System.Windows.Forms.PictureBox picDemo3;
    private System.Windows.Forms.PictureBox picDemo1;
    private System.Windows.Forms.GroupBox grpVotes;
    private System.Windows.Forms.Button btnVoteStudent3;
    private System.Windows.Forms.Button btnVoteStudent2;
    private System.Windows.Forms.Button btnVoteStudent1;
    private System.Windows.Forms.Label lblStudent3;
    private System.Windows.Forms.Label lblStudent2;
    private System.Windows.Forms.Label lblStudent1;
    private System.Windows.Forms.PictureBox pictureBox3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Button btnVorige;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label lblMyVotes;
    private System.Windows.Forms.Label lblMyVotes2;
    private System.Windows.Forms.Label lblNaamGameDemo;
    private System.Windows.Forms.Button btnMyVotes;
    private System.Windows.Forms.Label lblWelkom;
  }
}
