﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeVotingApp
{
 public class Student
  {
   protected string mVoornaam, mAchternaam;
   protected int mOntvangenVotes;
   protected bool mAlGevote = false;

   public Student(string voornaam, string achternaam)
   {
     mVoornaam = voornaam;
     mAchternaam = achternaam;
     mOntvangenVotes = 0;
   }

   public int OntvangenVotes
   {
     get { return mOntvangenVotes; }
     set { mOntvangenVotes = value; }
   }
   public string Voornaam
   {
     get { return mVoornaam;}
   }
   public string Achternaam
   {
     get { return mAchternaam; }
   }
   public bool AlGevote
   {
     get { return mAlGevote; }
     set { mAlGevote = value; }
   }
  }
}
