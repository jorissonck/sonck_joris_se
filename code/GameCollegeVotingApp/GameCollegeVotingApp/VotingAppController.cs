﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeVotingApp
{
 public class VotingAppController
  {

   protected VotingApp votingapp;
   protected GameDemo activeDemo;
   private string mVotes ;

   public VotingAppController()
   {
     votingapp = new VotingApp();
   }
   public GameDemo ActiveDemo
   {
     get { return activeDemo; }
   }

   public void CreateBezoeker(string email, string pw)
   {
     votingapp.Aanmaken(email, pw);
   }
   public void Vote(int studentnummer)
   {
     votingapp.Vote(activeDemo, studentnummer);

     mVotes += votingapp.VotedOnStudents()[votingapp.MyVotes() - 1 ].Voornaam + " " + votingapp.VotedOnStudents()[votingapp.MyVotes() - 1].Achternaam + "\n";
   }

   public string ClickDemo(int demonummer, int studentnummer)
   {
     activeDemo = votingapp.GetDemos()[demonummer-1];
     return votingapp.GetStudentNames(activeDemo, studentnummer);
   }
   
   public bool AlreadyVoted(int stuNr)
   {
     return votingapp.AlGevote(activeDemo, stuNr);
   }
   public Bezoeker GetBezoeker()
   {
     return votingapp.GetBezoeker();
   }
   public bool MaxVotes()
   {
     if (votingapp.CheckMaxVotes() == true)
     {
       return true;
     }
     else return false;
   }
  
   public int UpdateMyVotes()
   {
     return votingapp.MyVotes();
   }

   public string VotedStudents()
   {     
     return mVotes;
   }


  }
}
