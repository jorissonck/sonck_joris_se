﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeVotingApp
{
  public class VotingApp
  {
    protected GameDemo demo1;
    protected GameDemo demo2;
    protected GameDemo demo3;
    protected GameDemo demo4;
    protected GameDemo demo5;
    protected GameDemo demo6;
    protected List<GameDemo> demos;
    protected Student[] student = new Student[18];

    protected Bezoeker bezoeker;
    protected List<Student> votedOnStudents = new List<Student>();

    public VotingApp()
    {
      student[0] = new Student("Joris", "Sonck");
      student[1] = new Student("Jan", "Peeters");
      student[2] = new Student("Luc", "Verhoven");
      student[3] = new Student("Mark", "Verstreaten");
      student[4] = new Student("Ludo", "Rems");
      student[5] = new Student("Frank", "Verboven");
      student[6] = new Student("Steve", "Sorkens");
      student[7] = new Student("Glenn", "Van Damme");
      student[8] = new Student("Jean-Claude", "Vranckx");
      student[9] = new Student("Elias", "Vandersmissen");
      student[10] = new Student("Boris", "Smits");
      student[11] = new Student("Filip", "Borms");
      student[12] = new Student("Marc", "Parridaens");
      student[13] = new Student("Ellen", "Vanbeeck");
      student[14] = new Student("Rita", "Verschuren");
      student[15] = new Student("Gert", "Verhulst");
      student[16] = new Student("Nicolas", "Krekeltjes");
      student[17] = new Student("Yannick", "Warzee");

      demo1 = new GameDemo(student[0], student[1], student[2], "Angry Birds");
      demo2 = new GameDemo(student[3], student[4], student[5], "Space Invaders");
      demo3 = new GameDemo(student[6], student[7], student[8], "Tower Defense");
      demo4 = new GameDemo(student[9], student[10], student[11], "Cookie Clicker");
      demo5 = new GameDemo(student[12], student[13], student[14], "Minion Rush");
      demo6 = new GameDemo(student[15], student[16], student[17], "Tetris");

      demos = new List<GameDemo>();
      demos.Add(demo1);
      demos.Add(demo2);
      demos.Add(demo3);
      demos.Add(demo4);
      demos.Add(demo5);
      demos.Add(demo6);

    } 
    public List<Student> VotedOnStudents()
    {
      return votedOnStudents;
    }
     public void Aanmaken(string email, string wachtwoord)
    {
      bezoeker = new Bezoeker(email, wachtwoord);
    }

    public void Vote(GameDemo demo, int stunr)
     {
       demo.GetStudents()[stunr - 1].OntvangenVotes++;
       votedOnStudents.Add(demo.GetStudents()[stunr - 1]);
       demo.GetStudents()[stunr - 1].AlGevote = true;
       bezoeker.Votes++;
     }
    public string GetStudentNames(GameDemo demo, int StuNr)
    {
      string naam = " ";
      naam = demo.GetStudents()[StuNr - 1].Voornaam + demo.GetStudents()[StuNr - 1].Achternaam;
      return naam;
    }
   
    public bool CheckMaxVotes()
    {
      if (bezoeker.Votes >= bezoeker.MaxVotes)
      {
        return true;
      }
      else return false;
    }
    public bool AlGevote(GameDemo activeD, int stunr)
    {
      if (activeD.GetStudents()[stunr - 1].AlGevote == true)
      {
        return true;
      }
      else return false;
      
    }
    public Bezoeker GetBezoeker()
    {
      return bezoeker;
    }
    public int MyVotes()
    {
      return bezoeker.Votes;
    }
    public List<Student> GetStudents(GameDemo activedemo)
    {
      return activedemo.GetStudents();
    }
    public List<GameDemo> GetDemos()
    {
      return demos;
    }
  }
}
