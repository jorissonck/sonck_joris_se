﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollegeVotingApp
{
  public partial class VoteAppUI : UserControl
  {
    private VotingAppController controller;
    public VoteAppUI()
    {
      InitializeComponent();
      controller = new VotingAppController();
      grpRegistreren.Location = new Point(0, 0);
      grpGamedemos.Location = new Point(0, 0);
      grpGegevens.Location = new Point(0, 0);
      grpVotes.Location = new Point(0, 0);
    }
    

    private void btnRegistreren_Click(object sender, EventArgs e)
    {
      grpRegistreren.Visible = false;
      grpGegevens.Visible = true;
      
    }

    private void btnAccountAanmaken_Click(object sender, EventArgs e)
    {
      if (txtEmail.Text != "" && txtWachtwoord.Text != "")
      {
        grpGegevens.Visible = false;
        controller.CreateBezoeker(txtEmail.Text, txtWachtwoord.Text);
        lblWelkom.Text = "Welkom, " + controller.GetBezoeker().Email;
        grpGamedemos.Visible = true;
      }
      else
      {
        lblError.Visible = true;
      }
      

    }

    

    private void btnVorige_Click(object sender, EventArgs e)
    {
      SwitchVisible();
    }


    private void picDemo1_Click(object sender, EventArgs e)
    {
      SwitchVisible();
      UpdateNames(1);
      UpdateUI();
    }    

    private void picDemo2_Click(object sender, EventArgs e)
    {
      SwitchVisible();
      UpdateNames(2);
      UpdateUI();
    }

    private void picDemo3_Click(object sender, EventArgs e)
    {
      SwitchVisible();
      UpdateNames(3);
      UpdateUI();
    }

    private void picDemo4_Click(object sender, EventArgs e)
    {
      SwitchVisible();
      UpdateNames(4);
      UpdateUI();
    }

    private void picDemo5_Click(object sender, EventArgs e)
    {
      SwitchVisible();
      UpdateNames(5);
      UpdateUI();
    }

    private void picDemo6_Click(object sender, EventArgs e)
    {
      SwitchVisible();
      UpdateNames(6);
      UpdateUI();
    }



    private void btnVoteStudent1_Click(object sender, EventArgs e)
    {
      UpdateUI();
      if (controller.MaxVotes() == false)
      {
        if (ErrorAlGevote(1) == false)
        {
          controller.Vote(1);
        }
        else
        {
          MessageBox.Show("Het maximaal aantal stemmen op een student bedraagt 1!", "Oeps..");
        }       
      }
      UpdateUI();
    }

    private void btnVoteStudent2_Click(object sender, EventArgs e)
    {
      UpdateUI();
      if (controller.MaxVotes() == false)
      {
        if (ErrorAlGevote(2) == false)
        {
          controller.Vote(2);
        }
        else
        {
          MessageBox.Show("Het maximaal aantal stemmen op een student bedraagt 1!", "Oeps..");
        }       
      }
      
      UpdateUI();
    }

    private void btnVoteStudent3_Click(object sender, EventArgs e)
    {
      UpdateUI();
      if (controller.MaxVotes() == false)
      {
        if (ErrorAlGevote(3) == false)
        {
          controller.Vote(3);
        }
        else
        {
          MessageBox.Show("Het maximaal aantal stemmen op een student bedraagt 1!", "Oeps..");
        }       
      }
  
      UpdateUI();
    }

    private void UpdateUI()
    {
      DisableVoteBtns();
      UpdateLabels();
    }
    private void DisableVoteBtns()
    {
      if (controller.MaxVotes() == true)
      {
        btnVoteStudent1.Enabled = false;
        btnVoteStudent2.Enabled = false;
        btnVoteStudent3.Enabled = false;

        btnVoteStudent1.BackColor = Color.Gray;
        btnVoteStudent2.BackColor = Color.Gray;
        btnVoteStudent3.BackColor = Color.Gray;

      }
    }
    private void UpdateNames(int demonr)
    {
      lblStudent1.Text = controller.ClickDemo(demonr, 1);
      lblStudent2.Text = controller.ClickDemo(demonr, 2);
      lblStudent3.Text = controller.ClickDemo(demonr, 3);
    }
    private void SwitchVisible()
    {
      
      if (grpGamedemos.Visible == true)
      {
        grpGamedemos.Visible = false;
        grpVotes.Visible = true;
      }
      else
      {      
        grpGamedemos.Visible = true;
        grpVotes.Visible = false;
      }
    }
    private void UpdateLabels()
    {
      lblMyVotes.Text = ("Votes: " + Convert.ToString(controller.UpdateMyVotes()) + "/3");
      lblMyVotes2.Text = lblMyVotes.Text;
      lblNaamGameDemo.Text = controller.ActiveDemo.Naam;
      
    }
    private bool ErrorAlGevote(int student)
    {
      if (controller.AlreadyVoted(student) == true)
      {
        return true;
      }
      else return false;
    }

    private void btnMyVotes_Click(object sender, EventArgs e)
    {
      MessageBox.Show("U stemde voor: \n" + controller.VotedStudents(), "Mijn Votes");
    }

  }
}
